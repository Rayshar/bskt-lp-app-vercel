import MetaMaskOnboarding from '@metamask/onboarding';

import { Button } from '../Button';

export const OnboardingButton = () => {
  const onboarding = new MetaMaskOnboarding();

  return (
    <>
      <Button onClick={onboarding.startOnboarding}>
        Click to install MetaMask
      </Button>
    </>
  );
};
