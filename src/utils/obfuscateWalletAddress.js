export const obfuscateWalletAddress = (address = '') => {
  const addressLength = address.length;
  const firstSix = address.substring(0, 6);
  const lastSix = address.substring(addressLength - 4, addressLength);

  return `${firstSix}...${lastSix}`;
};
