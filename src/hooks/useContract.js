import { useEffect, useState } from 'react';
import Web3 from 'web3';
import { useABI } from './useABI';

export const useContract = ({ contractAddress = '', ropsten = false }) => {
  const [contract, setContract] = useState(null);
  const web3 = new Web3(Web3.givenProvider);
  const ABI = useABI({ contractAddress, ropsten });

  useEffect(() => {
    if (ABI) {
      setContract(new web3.eth.Contract(JSON.parse(ABI), contractAddress));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ABI]);

  return contract;
};
