import { useEffect, useState } from 'react';
import { normalizeBalance } from '../../utils';

export const TotalSupply = ({ contract, accountHash }) => {
  const [totalSupply, setTotalSupply] = useState('-');

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .totalSupply()
        .call({ from: accountHash })
        .then(setTotalSupply);
    }
  }, [accountHash, contract]);

  return (
    <>
      <div>Total Supply</div>
      <div
        style={{
          fontSize: '2rem',
          fontWeight: 'bold',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
        }}
        title={normalizeBalance(totalSupply)}
      >
        {normalizeBalance(totalSupply)}
      </div>
    </>
  );
};
