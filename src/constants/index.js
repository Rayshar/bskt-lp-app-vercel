// export const contractAddress = '0x0F58c61B4bA65b02E18F53A1c13BFd105bd61090';
export const contractAddress = '0x4CD15424097713d909C99d3E8413489959d188f5';
export const etherScanApiKey = 'KCYX24RRPQU2KBPGD9353EMTARJE8KIZ6Z';
export const defiPulseApiKey =
  'd9b6453fe3d50618908189ccbe1c221066369f1e4221e0af11a67c5b5172';
export const tokenName = 'UNI-V2 BSKT-ETH';
export const coinDecimalPlaces = 18;
export const gas = 250000; // maximal gas amount
export const gasPrice = '60000000000'; // maximal gas price

/**
 * Testnet (Ropsten) contract addresses
 */
export const tokenContractAddressRopsten =
  '0x94400f9d8234b8dace6821423792802d3f42798c';
export const stakerContractAddressRopsten =
  '0xc78bE2649e03ec6f4978f60c2A0f7Ca316cccb8C';
