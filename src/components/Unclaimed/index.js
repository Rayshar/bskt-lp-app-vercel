import { useEffect, useState } from 'react';
import { normalizeBalance, interactWithContract } from '../../utils';
import styles from './Unclaimed.module.css';
import { TransactionConfirmationModal } from '../TransactionConfirmationModal';
import { Prompt } from '../Prompt';

let interval = null;

export const Unclaimed = ({
  contract,
  accountHash,
  setPendingTransactionHash,
  pendingTransactionHash,
  onButtonClick,
}) => {
  const [unclaimed, setUnclaimed] = useState('-');
  const [confirmedTransaction, setConfirmedTransaction] = useState('');
  const [showClaimConfirmation, setShowClaimConfirmation] = useState(false);

  useEffect(() => {
    if (contract && accountHash) {
      window.clearInterval(interval);

      contract.methods.getReward(accountHash).call().then(setUnclaimed);

      interval = setInterval(() => {
        contract.methods.getReward(accountHash).call().then(setUnclaimed);
      }, 1000 * 20);
    }
  }, [contract, accountHash, pendingTransactionHash]);

  return (
    <>
      <div>Unclaimed</div>

      <div
        style={{
          fontSize: '2rem',
          fontWeight: 'bold',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
        }}
        title={normalizeBalance(unclaimed)}
      >
        {normalizeBalance(unclaimed)}
      </div>

      {unclaimed > 0 && (
        <button
          disabled={pendingTransactionHash}
          className={styles.unclaimBtn}
          onClick={() => {
            setShowClaimConfirmation(true);
          }}
        >
          Claim
        </button>
      )}

      <Prompt
        title="Please confirm operation"
        show={showClaimConfirmation}
        onClose={() => {
          setShowClaimConfirmation(false);
        }}
        onCancel={() => {
          setShowClaimConfirmation(false);
        }}
        onConfirm={() => {
          interactWithContract({
            contract,
            accountAddress: accountHash,
            methodName: 'claim',
            operationName: 'send',
            onConfirmation: (confirmationNumber, receipt) => {
              setConfirmedTransaction(receipt.transactionHash);
              setPendingTransactionHash('');
            },
            onTransactionHash: (hash) => {
              setPendingTransactionHash(hash);
            },
            onError: console.error,
          });

          setShowClaimConfirmation(false);
        }}
      >
        Each operation will cost you 2% of your transaction value.
      </Prompt>

      <TransactionConfirmationModal
        hash={confirmedTransaction}
        onClose={() => {
          setConfirmedTransaction('');
        }}
      />
    </>
  );
};
