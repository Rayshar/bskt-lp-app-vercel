import { Input } from '../Input';
import { Modal } from '../Modal';
import { Button } from '../Button';

export const UnstakeModal = () => {
  return (
    <Modal title={'Unstake'} show>
      <Input
        label="Enter amount to unstake"
        onChange={(value) => {
          console.log('Stake amount', value);
        }}
      />

      <Button
        onClick={() => {
          console.log('Unstake');
        }}
      >
        Unstake
      </Button>
    </Modal>
  );
};
