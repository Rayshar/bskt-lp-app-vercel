import { useEffect, useState } from 'react';
import { coinDecimalPlaces, stakerContractAddressRopsten } from '../constants';

export const useApprove = ({
  tokenContract,
  currentAccountHash,
  setPendingTransactionHash,
}) => {
  const [isAccountApproved, setIsAccountApproved] = useState(false);
  const [shouldAskForApproval, setShouldAskForApproval] = useState(false);

  /**
   * Check allowance
   */
  useEffect(() => {
    if (tokenContract && currentAccountHash) {
      tokenContract.methods
        .allowance(currentAccountHash, stakerContractAddressRopsten)
        .call({ from: currentAccountHash })
        .then((allowance) => {
          tokenContract.methods
            .balanceOf(currentAccountHash)
            .call()
            .then((balance) => {
              console.log(
                'is allowed',
                window.BigInt(allowance) >= window.BigInt(balance),
              );
              if (window.BigInt(allowance) >= window.BigInt(balance)) {
                setIsAccountApproved(true);
                setShouldAskForApproval(false);
              } else {
                setIsAccountApproved(false);
                setShouldAskForApproval(true);
              }
            });
        });
    }
  }, [currentAccountHash, tokenContract]);

  /**
   * Ask for approval
   */
  useEffect(() => {
    if (
      tokenContract &&
      currentAccountHash &&
      !isAccountApproved &&
      shouldAskForApproval
    ) {
      tokenContract.methods
        .approve(
          stakerContractAddressRopsten,
          window.BigInt(1000000000 * `1e+${coinDecimalPlaces}`),
        )
        .send({ from: currentAccountHash })
        .on('transactionHash', (hash) => {
          setPendingTransactionHash(hash);
        })
        .on('confirmation', (confirmationNumber, receipt) => {
          setIsAccountApproved(true);
          setPendingTransactionHash('');
        });
    }
  }, [
    tokenContract,
    currentAccountHash,
    isAccountApproved,
    shouldAskForApproval,
    setPendingTransactionHash,
  ]);

  return isAccountApproved;
};
