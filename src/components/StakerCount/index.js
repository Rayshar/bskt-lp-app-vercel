import { useEffect, useState } from 'react';

import styles from './StakerCount.module.css';

export const StakerCount = ({
  contract,
  accountHash,
  pendingTransactionHash,
}) => {
  const [stakerCount, setStakerCount] = useState('-');

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods.getStakerCount().call().then(setStakerCount);
    }
  }, [accountHash, contract, pendingTransactionHash]);

  return (
    <>
      <span>Stakers Count</span>
      <strong className={styles.count}>{stakerCount}</strong>
    </>
  );
};
