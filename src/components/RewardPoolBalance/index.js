import { useEffect, useState } from 'react';
import { normalizeBalance } from '../../utils';

import styles from './RewardPoolBalance.module.css';

export const RewardPoolBalance = ({
  contract,
  accountHash,
  pendingTransactionHash,
}) => {
  const [poolBalance, setPoolBalance] = useState('-');

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .getRewardPoolBalance()
        .call()
        .then((receipt) => {
          setPoolBalance(receipt);
        });
    }
  }, [accountHash, contract, pendingTransactionHash]);

  return (
    <>
      <span>Reward Pool Balance</span>
      <strong className={styles.balance}>
        {normalizeBalance(poolBalance)}
      </strong>
    </>
  );
};
