import { useEffect, useState } from 'react';
import { tokenName } from '../../constants';
import { normalizeBalance } from '../../utils';

import styles from './CoinBalance.module.css';

export const CoinBalance = ({
  contract,
  accountHash,
  pendingTransactionHash,
}) => {
  const [coinBalance, setCoinBalance] = useState(0);

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .balanceOf(accountHash)
        .call()
        .then((receipt) => {
          setCoinBalance(receipt);
        });
    } else {
      setCoinBalance(0);
    }
  }, [accountHash, contract, pendingTransactionHash]);

  return (
    <div className={styles.container}>
      <strong>{normalizeBalance(coinBalance)}</strong> {tokenName}
    </div>
  );
};
