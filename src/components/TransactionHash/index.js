import styles from './TransactionHash.module.css';
import { TransactionLink } from '../TransactionLink';
import { Loader } from '../Loader';

export const TransactionHash = ({ hash }) => {
  return (
    <>
      <div className={styles.container}>
        Last Pending Transaction:
        <br />
        <br />
        <TransactionLink hash={hash} />
        <br />
        <Loader />
      </div>
    </>
  );
};
