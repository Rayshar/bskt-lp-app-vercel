import { useEffect, useRef, useState } from 'react';
import { getWei, interactWithContract } from '../../utils';
import { Button } from '../Button';
import styles from './StakingForm.module.css';
import { TransactionConfirmationModal } from '../TransactionConfirmationModal';

export const StakingForm = ({
  stakerContract,
  accountHash,
  setPendingTransactionHash,
  pendingTransactionHash,
  hasApproval,
}) => {
  const [stakeAmount, setStakeAmount] = useState('');
  const [confirmedTransaction, setConfirmedTransaction] = useState();
  const inputRef = useRef();

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const stake = () => {
    interactWithContract({
      contract: stakerContract,
      accountAddress: accountHash,
      methodName: 'stake',
      methodValue: getWei(stakeAmount),
      operationName: 'send',
      operationParams: {},
      onConfirmation: (confirmationNumber, receipt) => {
        setConfirmedTransaction(receipt.transactionHash);
        setPendingTransactionHash('');
      },
      onTransactionHash: (hash) => {
        setPendingTransactionHash(hash);
      },
      onError: (error) => {
        console.error(error);
      },
    });

    setStakeAmount('');
  };

  return (
    <>
      <span>How much liquidity would you like to provide?</span>

      <input
        className={styles.input}
        type="number"
        min={0}
        value={stakeAmount}
        onChange={(e) => {
          setStakeAmount(e.target.value);
        }}
        ref={inputRef}
      />

      <Button
        onClick={stake}
        className={styles.btn}
        disabled={pendingTransactionHash || !stakeAmount || !hasApproval}
      >
        {hasApproval
          ? 'Stake UNI-V2 LP tokens'
          : 'You need confirmed approval to provide liquidity'}
      </Button>

      <TransactionConfirmationModal
        hash={confirmedTransaction}
        onClose={() => {
          setConfirmedTransaction('');
        }}
      />
    </>
  );
};
