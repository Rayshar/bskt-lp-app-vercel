import { useState, useEffect } from 'react';
import { isMetaMaskInstalled } from '../utils';

export const useEvents = (getAccountsAsync) => {
  const [state, setState] = useState({ type: null, data: {} });

  useEffect(() => {
    if (isMetaMaskInstalled()) {
      window.ethereum.on('accountsChanged', (accounts) => {
        console.log('accountChanged', accounts);
        setState({
          type: 'accountsChanged',
          data: {
            accounts,
          },
        });
      });

      window.ethereum.on('chainChanged', (chainId) => {
        window.location.reload();
      });

      window.ethereum.on('connect', (connectInfo) => {
        console.log('connect', connectInfo);
        setState({
          type: 'connect',
          data: {},
        });
      });

      window.ethereum.on('disconnect', (error) => {
        setState({
          type: 'disconnect',
          data: {
            error,
          },
        });
      });

      window.ethereum.on('message', (message) => {
        setState({
          type: 'message',
          data: {
            message,
          },
        });
      });
    }
  }, []);

  return state;
};
