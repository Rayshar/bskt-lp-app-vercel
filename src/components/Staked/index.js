import { useEffect, useState } from 'react';
import { normalizeBalance, getWei, interactWithContract } from '../../utils';
import styles from './Staked.module.css';
import { Modal } from '../Modal';
import { Input } from '../Input';
import { Button } from '../Button';
import { TransactionConfirmationModal } from '../TransactionConfirmationModal';

export const Staked = ({
  contract,
  accountHash,
  setPendingTransactionHash,
  pendingTransactionHash,
  onButtonClick = () => {},
}) => {
  const [staked, setStake] = useState('-');
  const [valueToUnstake, setValueToUnstake] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [confirmedTransaction, setConfirmedTransaction] = useState('');

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .getStakedAmount(accountHash)
        .call()
        .then(setStake)
        .catch((error) => {
          console.error('[ERROR: Staked (getStakedAmount)]:', error);
        });
    }
  }, [accountHash, contract, pendingTransactionHash]);

  return (
    <>
      <div>Staked</div>
      <div
        style={{
          fontSize: '2rem',
          fontWeight: 'bold',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
        }}
        title={normalizeBalance(staked)}
      >
        {normalizeBalance(staked)}
      </div>

      {staked > 0 && (
        <button
          className={styles.claimBtn}
          disabled={pendingTransactionHash}
          onClick={() => {
            setShowModal(true);
            onButtonClick(staked);
          }}
        >
          Unstake
        </button>
      )}

      {staked > 0 && (
        <>
          <button
            title="This function withdraws all staked balance and automatically claims all rewards"
            className={styles.endStakeBtn}
            disabled={pendingTransactionHash}
            onClick={() => {
              interactWithContract({
                contract,
                accountAddress: accountHash,
                methodName: 'endStake',
                operationName: 'send',
                onConfirmation: (confirmationNumber, receipt) => {
                  setConfirmedTransaction(receipt.transactionHash);
                  setPendingTransactionHash('');
                },
                onTransactionHash: (hash) => {
                  setPendingTransactionHash(hash);
                },
                onError: (error) => {
                  console.error('[ERROR Staked (unstake)]:', error);
                },
              });
            }}
          >
            Withdraw All
          </button>
        </>
      )}

      <Modal
        title={'Unstake'}
        show={showModal}
        onClose={() => {
          setShowModal(false);
        }}
      >
        <Input
          label="Enter amount to unstake"
          onChange={setValueToUnstake}
          className={styles.input}
          value={valueToUnstake}
        />
        <Button
          onClick={() => {
            setShowModal(false);
            setValueToUnstake('');
            interactWithContract({
              contract,
              accountAddress: accountHash,
              methodName: 'unstake',
              methodValue: getWei(valueToUnstake),
              operationName: 'send',
              onConfirmation: (confirmationNumber, receipt) => {
                setConfirmedTransaction(receipt.transactionHash);
                setPendingTransactionHash('');
              },
              onTransactionHash: (hash) => {
                setPendingTransactionHash(hash);
              },
              onError: (error) => {
                console.error('[ERROR Staked (unstake)]:', error);
              },
            });
          }}
        >
          Unstake
        </Button>
      </Modal>

      <TransactionConfirmationModal
        hash={confirmedTransaction}
        onClose={() => {
          setConfirmedTransaction('');
        }}
      />
    </>
  );
};
