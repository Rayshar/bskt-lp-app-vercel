import { useEffect, useState } from 'react';

export const useABI = ({ contractAddress, ropsten = false }) => {
  const [ABI, setABI] = useState('');

  useEffect(() => {
    if (contractAddress) {
      window
        .fetch(
          `https://api${
            ropsten ? '-ropsten' : ''
          }.etherscan.io/api?module=contract&action=getabi&address=${contractAddress}&apikey=KCYX24RRPQU2KBPGD9353EMTARJE8KIZ6Z`,
        )
        .then((data) => data.json())
        .then((data) => setABI(data.result))
        .catch((error) => {
          console.error('[useABI hook]: ', error);
        });

      return;
    }

    throw new Error('[useABI hook]: contractAddress has to be defined.');
  }, [contractAddress, ropsten]);

  return ABI;
};
