import { Modal } from '../Modal';
import { TransactionLink } from '../TransactionLink';

export const TransactionConfirmationModal = ({ onClose, hash }) => {
  return (
    <Modal onClose={onClose} title={'Transaction confirmed'} show={hash}>
      <TransactionLink hash={hash} />
    </Modal>
  );
};
