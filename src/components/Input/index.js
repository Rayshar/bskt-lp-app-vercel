import styles from './Input.module.css';

export const Input = ({ onChange, label, inputRef, className = '', value }) => {
  return (
    <div className={styles.container}>
      {label && <label className={styles.label}>{label}</label>}

      <input
        className={`${styles.input} ${className}`}
        type="number"
        value={value}
        onChange={(e) => {
          onChange(e.target.value);
        }}
        {...(inputRef ? { ref: inputRef } : {})}
      />
    </div>
  );
};
