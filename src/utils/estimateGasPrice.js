import { defiPulseApiKey } from '../constants';

export const estimateGasPrice = () => {
  return window.fetch(
    `https://data-api.defipulse.com/api/v1/egs/api/ethgasAPI.json?api-key=${defiPulseApiKey}`,
  );
};
