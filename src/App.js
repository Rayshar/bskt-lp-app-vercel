import './App.css';
import { useEffect, useState } from 'react';
import Particles from 'react-particles-js';

import { isMetaMaskInstalled } from './utils';
import {
  AccountHash,
  ConnectToMetaMaskButton,
  OnboardingButton,
  TopBar,
  Main,
  Card,
  CoinBalance,
  Staked,
  Unclaimed,
  StakingForm,
  TransactionHash,
  RewardPoolBalance,
  StakerCount,
} from './components';
import mainStyles from './components/Main/Main.module.css';
import { useEvents, useContract } from './hooks';
import {
  stakerContractAddressRopsten,
  tokenContractAddressRopsten,
} from './constants';
import { useApprove } from './hooks/useApprove';
import { useMedia } from './hooks/useMedia';

function App() {
  const particlesCount = useMedia(['(max-width: 1024px)'], [50], 100);
  const [currentAccountHash, setCurrentAccountHash] = useState(null);
  const [pendingTransactionHash, setPendingTransactionHash] = useState('');
  const walletState = useEvents();
  const tokenContract = useContract({
    contractAddress: tokenContractAddressRopsten,
    ropsten: true,
  });
  const stakerContract = useContract({
    contractAddress: stakerContractAddressRopsten,
    ropsten: true,
  });
  const hasApproval = useApprove({
    tokenContract,
    currentAccountHash,
    setPendingTransactionHash,
  });

  useEffect(() => {
    if (
      walletState.data &&
      walletState.data.accounts &&
      walletState.data.accounts.length
    ) {
      setCurrentAccountHash(walletState.data.accounts[0]);
    }
  }, [walletState]);

  return (
    <div className="App">
      <TopBar>
        <CoinBalance
          contract={tokenContract}
          accountHash={currentAccountHash}
          pendingTransactionHash={pendingTransactionHash}
        />

        {!isMetaMaskInstalled() ? (
          <OnboardingButton />
        ) : (
          <>
            {!currentAccountHash && (
              <ConnectToMetaMaskButton
                setCurrentAccountHash={setCurrentAccountHash}
              >
                Connect to a wallet
              </ConnectToMetaMaskButton>
            )}

            {currentAccountHash && <AccountHash hash={currentAccountHash} />}
          </>
        )}
      </TopBar>

      <Main>
        <Particles
          style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            zIndex: -1,
          }}
          params={{
            particles: {
              number: {
                value: particlesCount,
              },
              size: {
                value: 5,
              },
              opacity: 0.1,
            },
            interactivity: {
              events: {
                onhover: {
                  enable: true,
                  mode: 'repulse',
                },
              },
            },
          }}
        />

        <div className={mainStyles.rewardPoolBalance}>
          <Card>
            <RewardPoolBalance
              contract={stakerContract}
              accountHash={currentAccountHash}
              pendingTransactionHash={pendingTransactionHash}
            />
          </Card>
        </div>

        <div className={mainStyles.stakerCount}>
          <Card>
            <StakerCount
              contract={stakerContract}
              accountHash={currentAccountHash}
              pendingTransactionHash={pendingTransactionHash}
            />
          </Card>
        </div>

        <div className={mainStyles.totalDeposits}>
          <Card>
            <Staked
              contract={stakerContract}
              accountHash={currentAccountHash}
              setPendingTransactionHash={setPendingTransactionHash}
              pendingTransactionHash={pendingTransactionHash}
            />
          </Card>
        </div>

        <div className={mainStyles.unclaimedRewards}>
          <Card>
            <Unclaimed
              contract={stakerContract}
              accountHash={currentAccountHash}
              setPendingTransactionHash={setPendingTransactionHash}
              pendingTransactionHash={pendingTransactionHash}
              onButtonClick={(claim) => {
                console.log('claim', claim);
              }}
            />
          </Card>
        </div>

        <div className={mainStyles.stake}>
          {stakerContract && currentAccountHash && (
            <Card>
              <StakingForm
                hasApproval={hasApproval}
                tokenContract={tokenContract}
                stakerContract={stakerContract}
                accountHash={currentAccountHash}
                setPendingTransactionHash={setPendingTransactionHash}
                pendingTransactionHash={pendingTransactionHash}
              />
            </Card>
          )}
        </div>

        {pendingTransactionHash && (
          <div className={mainStyles.pendingTransaction}>
            <TransactionHash hash={pendingTransactionHash} />
          </div>
        )}
      </Main>
    </div>
  );
}

export default App;
