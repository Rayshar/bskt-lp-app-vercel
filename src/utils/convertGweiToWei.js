export const convertGweiToWei = (gwei) => {
  return gwei * 1e9;
};
