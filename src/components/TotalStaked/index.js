import { useEffect, useState } from 'react';
import { normalizeBalance } from '../../utils';

export const TotalStaked = ({
  contract,
  accountHash,
  pendingTransactionHash,
}) => {
  const [totalStaked, setTotalStaked] = useState('-');

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .getTotalStakedAmount()
        .call({ from: accountHash })
        .then(setTotalStaked)
        .catch((error) => {
          console.error('[TotalStaked (getTotalStakedAmount)]:', error);
        });
    }
  }, [accountHash, contract, pendingTransactionHash]);

  return (
    <>
      <div>Total Deposits</div>
      <div
        style={{
          fontSize: '2rem',
          fontWeight: 'bold',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
        }}
        title={normalizeBalance(totalStaked)}
      >
        {normalizeBalance(totalStaked)}
      </div>
    </>
  );
};
