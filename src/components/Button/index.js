import styles from './Button.module.css';

export const Button = ({
  children,
  className = '',
  disabled,
  onClick = () => {},
}) => {
  return (
    <button
      disabled={disabled}
      className={`${styles.btn} ${className}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
